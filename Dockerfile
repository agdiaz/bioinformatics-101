FROM gcc:4.9

WORKDIR /src

COPY main.c /src/

RUN gcc -o /usr/bin/dna2rna main.c

ENTRYPOINT ["dna2rna"]