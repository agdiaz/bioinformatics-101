#include <stdio.h>
#include <string.h>

char transcript(char base) {
  char transcriptedBase;

  switch (base) {
    case 'a':
      transcriptedBase = 't';
      break;
    case 'A':
      transcriptedBase = 'T';
      break;
    case 'c':
      transcriptedBase = 'g';
      break;
    case 'C':
      transcriptedBase = 'G';
      break;
    case 'g':
      transcriptedBase = 'c';
      break;
    case 'G':
      transcriptedBase = 'C';
      break;
    case 't':
      transcriptedBase = 'a';
      break;
    case 'T':
      transcriptedBase = 'A';
      break;
    default:
      transcriptedBase = '?';
      break;
  }

  return transcriptedBase;
}

int main(int argc, char* argv[]) {
  unsigned int position;
  unsigned int sequenceLength;
  char* dnaSequence;

  dnaSequence = argv[1];
  sequenceLength = strlen(dnaSequence);
  char transcriptedRna[sequenceLength + 1];

  for (position = 0; position < sequenceLength; position++) {
    char currentValue = dnaSequence[position];

    transcriptedRna[position] = transcript(currentValue);
  }

  transcriptedRna[sequenceLength] = '\0';

  printf("%s\n", transcriptedRna);

  return 0;
}